<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Наши контакты - Лидер Лэнд");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Контакты");
?> 

<!--========================================================
                            CONTENT
  =========================================================-->

<main>        

    <section class="well well7">
        <div class="container">

            <div class="col-md-6 col-sm-12 col-xs-12 img-thumbnails wow" data-wow-duration='2s'>
                <h3 class="txt-sec">
                    Наши контакты
                </h3>
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 67px;">            
                    <address class="main_addr addr_full">
                        Время работы пн-вс. 09.00-21.00 <br>
                        Адрес нахождения базы МО Апрелевка
                    </address> 
                    <address class="addr1 addr_full">           
                        <dl>
                            <dt>Тел.:</dt>
                            <dd>
                                <a href="callto:#"> 8 495 363 70 60</a>
                            </dd>
                            <dt>Тел.:</dt>
                            <dd>
                                <a href="callto:#"> 8 964 778 21 00</a>
                            </dd>
                            <dt>Тел.:</dt>
                            <dd>
                                <a href="callto:#"> 8 903 144 89 05</a>
                            </dd>           
                        </dl>  
                        <dl class="mail">
                            <dt>E-mail:</dt>
                            <dd>
                                <a href="mailto:#">info@leader-land.ru</a>
                            </dd>
                        </dl>         
                    </address>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <h3 class="txt-sec">
                    Напишите нам
                </h3>
                <?
                    $APPLICATION->IncludeComponent("bitrix:iblock.element.add.form", "message", Array(
                        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                        "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                        "CUSTOM_TITLE_DETAIL_TEXT" => "Сообщение",
                        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                        "CUSTOM_TITLE_NAME" => "Имя",
                        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                        "CUSTOM_TITLE_TAGS" => "",
                        "DEFAULT_INPUT_SIZE" => "30",
                        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                        "ELEMENT_ASSOC" => "",
                        "ELEMENT_ASSOC_PROPERTY" => "",
                        "GROUPS" => array("2"),
                        "IBLOCK_ID" => "15",
                        "IBLOCK_TYPE" => "message",
                        "LEVEL_LAST" => "Y",
                        "LIST_URL" => "",
                        "MAX_FILE_SIZE" => "0",
                        "MAX_LEVELS" => "100000",
                        "MAX_USER_ENTRIES" => "100000",
                        "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
                        "PROPERTY_CODES" => array("NAME", "63", "64", "DETAIL_TEXT"),
                        "PROPERTY_CODES_REQUIRED" => array("NAME", "63", "64", "DETAIL_TEXT"),
                        "RESIZE_IMAGES" => "Y",
                        "SEF_FOLDER" => "/",
                        "SEF_MODE" => "Y",
                        "STATUS" => "ANY",
                        "STATUS_NEW" => "N",
                        "USER_MESSAGE_ADD" => "Ваше сообщение отправлено!",
                        "USER_MESSAGE_EDIT" => "",
                        "USE_CAPTCHA" => "N",
                        "VARIABLE_ALIASES" => Array()
                            )
                    );
                    ?>

            </div>

        </div>
    </section>
</main>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>