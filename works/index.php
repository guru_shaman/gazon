<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Озеленение и благоустройство территорий");
$APPLICATION->SetPageProperty("title", "Наши работы по озеленению и благоустройству - Лидер Лэнд");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Наши работы");
?> 

<!--========================================================
                            CONTENT
  =========================================================-->

<main>        

    <section class="well well7">
        <div class="container">
            <h1 class="txt-sec text-center">
                Наши работы по озеленению и благоустройству
            </h1>
            <div class="row offs4">

                <div class="col-sm-12 col-xs-12">
                    <ul class="navbar-nav">
                        <li <? if ($APPLICATION->GetCurPage() == '/works/'): ?>class="active"<? endif; ?>>
                            <a href="/works/">Все</a>        
                        </li>
                        <?
                        if (CModule::IncludeModule("iblock")):
                            $arFilter = array('IBLOCK_ID' => 14);
                            $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
                            while ($arSection = $rsSections->Fetch()):
                                ?>
                                <li <? if ($APPLICATION->GetCurPage() == '/works/' . $arSection['ID']): ?>class="active"<? endif; ?>>
                                    <a href="/works/<?= $arSection['ID'] ?>"><?= $arSection['NAME'] ?></a>        
                                </li>
                                <?
                            endwhile;
                        endif;
                        ?>
                    </ul>



                    <?
                    $APPLICATION->IncludeComponent(
                            "bitrix:news.list", "works", array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "works",
                        "IBLOCK_ID" => "14",
                        "NEWS_COUNT" => "16",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "ID",
                            1 => "DETAIL_PICTURE",
                            2 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "DESCRIPTION",
                            2 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "Y",
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_LAST_MODIFIED" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Работы",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "modern",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "Y",
                        "SET_STATUS_404" => "Y",
                        "SHOW_404" => "Y",
                        "MESSAGE_404" => "",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "COMPONENT_TEMPLATE" => "works",
                        "FILE_404" => ""
                            ), false
                    );
                    ?>
                </div>
            </div>

        </div>
    </section> 

</main>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>