<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Цены на рулонные газоны, уход за газонами, устройство газона");
$APPLICATION->SetPageProperty("title", "Цены на рулонные газоны - Лидер Лэнд");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Цены на рулонный газон");
?><!--========================================================
                           CONTENT
 =========================================================--> <main> <section class="well well7">
<div class="container">
	<h1 class="txt-sec text-center">
	Цены на рулонные газоны </h1>
	<div class="row offs4">
		<div class="col-sm-12 col-xs-12">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
					Наименование
				</th>
				<th>
					от 200 - до 1200 м2
				</th>
				<th>
					от 1200 - до 2500 м2
				</th>
				<th>
					от 2500 м2 и выше
				</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					Универсальный
				</td>
				<td>
					135 руб.
				</td>
				<td>
					130 руб.
				</td>
				<td>
					Договорная
				</td>
			</tr>
			<tr>
				<td>
					Элитный
				</td>
				<td>
					145 руб.
				</td>
				<td>
					140 руб.
				</td>
				<td>
					Договорная
				</td>
			</tr>
			<tr>
				<td>
					Спортивный
				</td>
				<td>
					160 руб.
				</td>
				<td>
					150 руб.
				</td>
				<td>
					Договорная
				</td>
			</tr>
			<tr>
				<td>
					Теневыносливый
				</td>
				<td>
					170 руб.
				</td>
				<td>
					160 руб.
				</td>
				<td>
					Договорная
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<h2 class="txt-sec text-center" style="font-size: 36px">
		Устройство газона «Под ключ» </h2>
		<div class="col-sm-12 col-xs-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"prices_main",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"FIELD_CODE" => Array("ID","DETAIL_PICTURE"),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "prices",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK" => "",
		"PAGER_BASE_LINK_ENABLE" => "Y",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => Array("PRICE"),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
		</div>
		<h2 class="txt-sec text-center" style="font-size: 36px">
		Сопутствующие товары </h2>
		<div class="col-sm-12 col-xs-12">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
					Товар
				</th>
				<th width="20%">
					Цена
				</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					Сетка от кротов Премиум (Г-8)
				</td>
				<td>
					142 руб. м2
				</td>
			</tr>
			<tr>
				<td>
					Грунт плодородный с доставкой
				</td>
				<td>
					1 200 руб. м3
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<h2 class="txt-sec text-center" style="font-size: 36px">
		Услуги по уходу за газоном </h2>
		<div class="col-sm-12 col-xs-12">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
					Услуга
				</th>
				<th width="20%">
					Цена
				</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					Покос
				</td>
				<td>
					от 500 руб./1 сотка
				</td>
			</tr>
			<tr>
				<td>
					Удобрение
				</td>
				<td>
					от 500 руб./1 сотка
				</td>
			</tr>
			<tr>
				<td>
					Гербициды (от сорняка)
				</td>
				<td>
					от 600 руб./1 сотка
				</td>
			</tr>
			<tr>
				<td>
					Вертикальная стрижка
				</td>
				<td>
					от 600 руб./1 сотка
				</td>
			</tr>
			<tr>
				<td>
					Аэрация
				</td>
				<td>
					от 600 руб./ 1 сотка
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<h3 class="txt-sec text-center">
		Перечень работ, выполняемых  сотрудниками нашей компании </h3>
		<div class="col-sm-12 col-xs-12">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
					Услуга
				</th>
				<th width="20%">
					Цена
				</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					Укладка рулонного газона
				</td>
				<td>
					от 60 руб. за 1 м2
				</td>
			</tr>
			<tr>
				<td>
					Подготовка почвы
				</td>
				<td>
					от 110 руб. за 1 м2
				</td>
			</tr>
			<tr>
				<td>
					Изменение высоты и уклона, устранение неровностей, создание ландшафта
				</td>
				<td>
					от 150 руб. за 1 м2
				</td>
			</tr>
			<tr>
				<td>
					Газон «под ключ» (полный цикл работ: снятие грунта, вывоз, выравнивание, уплотнение, устранение неровностей, укладка газона, вывоз мусора, наши рекомендации.)
				</td>
				<td>
					от 350 руб. за 1 м2
				</td>
			</tr>
			<tr>
				<td>
					Восстановление газона
				</td>
				<td>
					от 100 руб. м2
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
 </section> 
<section id="send-message" class="well well6 center767 parallax" data-url="<? print SITE_TEMPLATE_PATH; ?>/images/parallax_dots.jpg">

<div class="container">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<h3 class="txt-sec" style="color: #ffffff;">
			Напишите нам </h3>
 <article class="media">
			<form id="" class="contact-form">
				<div class="contact-form-loader">
				</div>
				<fieldset>
 <label class="name"> <input name="name" placeholder="Имя" value="" data-constraints="@Required @JustLetters" type="text"> </label> <br>
 <label class="email"> <input name="email" placeholder="Email" value="" data-constraints="@Required @Email" type="text"> </label> <br>
 <label class="phone"> <input name="phone" placeholder="Телефон" value="" data-constraints="@JustNumbers" type="text"> </label> <br>
 <label class="message"> <textarea name="message" placeholder="Сообщение" data-constraints="@Required @Length(min=20,max=999999)"></textarea> </label>
					<!--  <label class="recaptcha"> <span class="empty-message">*This field is required.</span> </label> -->
					<div class="btn-wr text-primary">
 <a class="btn btn-default" href="#" data-type="submit" style="color: #ffffff;">Отправить</a>
					</div>
				</fieldset>
				<div class="modal fade response-message">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								× </button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 You message has been sent! We will be in touch soon.
							</div>
						</div>
					</div>
				</div>
			</form>
 </article>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 img-thumbnails wow fadeInRight" data-wow-duration="2s">
			<h3 class="txt-sec" style="color: #ffffff;">
			Доставка </h3>
			<ul class="marked-list" style="margin-top: 67px; color: #ffffff;">
				<li>
				<a >
                                Мы осуществляем доставку любых объемов в любые регоны.
                            </a> </li>
				<li>
				<a >
                                Профессиональные водители быстро доставят груз, сохранив качество и первозданный вид.
                            </a> </li>
				<li>
				<a >
                                Наши менеджеры подберут наиболее выгодное предложение по доставке для вас!
                            </a> </li>
			</ul>
		</div>
	</div>
</div>
 </section> </main><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>