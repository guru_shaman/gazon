<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Лидер Лэнд");
$APPLICATION->SetPageProperty("description", "Рулонные газоны, рулонные газоны  «под ключ», рулонные газоны  в Москве");
$APPLICATION->SetPageProperty("title", "Рулонные газоны «под ключ» в Москве - Лидер Лэнд");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Лидер Лэнд");
?> <!--========================================================
CONTENT
=========================================================-->

<main>        


    <section class="camera_container">
        <div class="bot-blc">
            <div class="container">
                <h3 class=" wow fadeInLeft" data-wow-duration='2s'>
                    Рулонные газоны «под ключ»!
                </h3>
            </div>
        </div>

        <div class="jumbotron">
            <h2>
                Рулонный газон
                высшего качества!<br />
                <span style="font-size: 20px;line-height: 40px;">
                    В короткие сроки вы получите прекрасную лужайку,<br />
                    которая будет радовать вас каждый день</span><br />
            </h2>
            <a href="/prices/" class="btn btn-lg">подробнее</a>
        </div>
        <?
        $APPLICATION->IncludeComponent("bitrix:news.list", "slider_main", Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "Y",
            "IBLOCK_TYPE" => "slider",
            "IBLOCK_ID" => "12",
            "NEWS_COUNT" => "100",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
            "PROPERTY_CODE" => Array("DESCRIPTION"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "Y",
            "SET_BROWSER_TITLE" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_META_DESCRIPTION" => "Y",
            "SET_LAST_MODIFIED" => "Y",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            "ADD_SECTIONS_CHAIN" => "Y",
            "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "3600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "Y",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "Y",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "PAGER_BASE_LINK_ENABLE" => "Y",
            "SET_STATUS_404" => "Y",
            "SHOW_404" => "Y",
            "MESSAGE_404" => "",
            "PAGER_BASE_LINK" => "",
            "PAGER_PARAMS_NAME" => "arrPager",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
                )
        );
        ?>
    </section>

    <section class="well well1 parallax" data-url="<?= SITE_TEMPLATE_PATH ?>/images/parallax_dots.jpg" data-mobile="true">
  
        <?
        $APPLICATION->IncludeComponent("bitrix:news.detail", "main_page_1", Array(
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => "16",
            "ELEMENT_ID" => 370,
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "3600",
            "CACHE_GROUPS" => "Y",
                )
        );
        ?>
    </section>

    <?
    $APPLICATION->IncludeComponent("bitrix:news.list", "prices_main", Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "Y",
        "IBLOCK_TYPE" => "prices",
        "IBLOCK_ID" => "13",
        "NEWS_COUNT" => "100",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
        "PROPERTY_CODE" => Array("PRICE"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "Y",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "SET_LAST_MODIFIED" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
            )
    );
    ?>

    <section class="well well5" data-url="<?= SITE_TEMPLATE_PATH ?>/images/parallax3.jpg" data-mobile="true">
        <?
        $APPLICATION->IncludeComponent("bitrix:news.detail", "main_page_2", Array(
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => "16",
            "ELEMENT_ID" => 371,
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "3600",
            "CACHE_GROUPS" => "Y",
                )
        );
        ?>
    </section>

    <section id="send-message" class="well well6  center767">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <h3 class="txt-sec">
                        Напишите нам
                    </h3>

                    <?
                    $APPLICATION->IncludeComponent(
                            "bitrix:iblock.element.add.form", "message", array(
                        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                        "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                        "CUSTOM_TITLE_DETAIL_TEXT" => "Сообщение",
                        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                        "CUSTOM_TITLE_NAME" => "Имя",
                        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                        "CUSTOM_TITLE_TAGS" => "",
                        "DEFAULT_INPUT_SIZE" => "30",
                        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                        "ELEMENT_ASSOC" => "PROPERTY_ID",
                        "ELEMENT_ASSOC_PROPERTY" => "",
                        "GROUPS" => array(
                            0 => "2",
                        ),
                        "IBLOCK_ID" => "15",
                        "IBLOCK_TYPE" => "message",
                        "LEVEL_LAST" => "Y",
                        "LIST_URL" => "",
                        "MAX_FILE_SIZE" => "0",
                        "MAX_LEVELS" => "100000",
                        "MAX_USER_ENTRIES" => "100000",
                        "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
                        "PROPERTY_CODES" => array(
                            0 => "63",
                            1 => "64",
                            2 => "NAME",
                            3 => "DETAIL_TEXT",
                        ),
                        "PROPERTY_CODES_REQUIRED" => array(
                            0 => "63",
                            1 => "64",
                            2 => "NAME",
                            3 => "DETAIL_TEXT",
                        ),
                        "RESIZE_IMAGES" => "Y",
                        "SEF_FOLDER" => "/",
                        "SEF_MODE" => "Y",
                        "STATUS" => "ANY",
                        "STATUS_NEW" => "N",
                        "USER_MESSAGE_ADD" => "Ваше сообщение отправлено!",
                        "USER_MESSAGE_EDIT" => "",
                        "USE_CAPTCHA" => "N",
                        "COMPONENT_TEMPLATE" => "message"
                            ), false
                    );
                    ?>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12 img-thumbnails wow" data-wow-duration='2s'>
                    <h3 class="txt-sec">
                        Наши работы
                    </h3>
                    <?
                    $APPLICATION->IncludeComponent("bitrix:news.list", "works_main", Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "Y",
                        "IBLOCK_TYPE" => "works",
                        "IBLOCK_ID" => "14",
                        "NEWS_COUNT" => "4",
                        "SORT_BY1" => "RAND",
                        "SORT_ORDER1" => "RAND",
                        "SORT_BY2" => "RAND",
                        "SORT_ORDER2" => "RAND",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => Array("ID", "DETAIL_PICTURE"),
                        "PROPERTY_CODE" => Array("DESCRIPTION"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "Y",
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_LAST_MODIFIED" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "Y",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "PAGER_BASE_LINK_ENABLE" => "Y",
                        "SET_STATUS_404" => "Y",
                        "SHOW_404" => "Y",
                        "MESSAGE_404" => "",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                            )
                    );
                    ?>
                    <a href="/works/" class="btn btn-default">все работы</a>
                </div>

            </div>
        </div>
    </section>



</main>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>