<?
$MESS["REST_MODULE_NAME"] = "REST API";
$MESS["REST_MODULE_DESCRIPTION"] = "Програмний інтерфейс для зовнішніх і внутрішніх зістосунків";
$MESS["REST_INSTALL_TITLE"] = "Установка модуля \"REST API\"";
$MESS["REST_UNINSTALL_TITLE"] = "Видалення модуля \"REST API\"";
$MESS["REST_DB_NOT_SUPPORTED"] = "Модуль підтримує роботу тільки з СУБД MySQL";
?>