<?
$MESS["MFP_ALL_REQ"] = "(tous non obligatoires)";
$MESS["MFP_EMAIL_TO"] = "Adresse email sur lequel le message sera envoyé";
$MESS["MFP_NAME"] = "Dénomination";
$MESS["MFP_CAPTCHA"] = "Utiliser la protection contre les messages automatiques (CAPTCHA) pour les utilisateurs non autorisés";
$MESS["MFP_REQUIRED_FIELDS"] = "Champs obligatoires";
$MESS["MFP_EMAIL_TEMPLATES"] = "Modèles de courriers pour l'envoi de la lettre";
$MESS["MFP_MESSAGE"] = "Lettre";
$MESS["MFP_OK_MESSAGE"] = "Message sorti pour l'utilisateur après expédition";
$MESS["MFP_OK_TEXT"] = "Merci! Votre message est accepté.";
?>