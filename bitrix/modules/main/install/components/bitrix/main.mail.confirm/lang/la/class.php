<?
$MESS["MAIN_MAIL_CONFIRM_AUTH"] = "Error de autenticación";
$MESS["MAIN_MAIL_CONFIRM_AJAX_ERROR"] = "Error al procesar la solicitud";
$MESS["MAIN_MAIL_CONFIRM_MENU"] = "Agregar remitente";
$MESS["MAIN_MAIL_CONFIRM_TITLE"] = "Agregar nueva dirección";
$MESS["MAIN_MAIL_CONFIRM_GET_CODE"] = "Obtener código";
$MESS["MAIN_MAIL_CONFIRM_SAVE"] = "Guardar";
$MESS["MAIN_MAIL_CONFIRM_CANCEL"] = "Cancelar";
$MESS["MAIN_MAIL_CONFIRM_EMAIL_HINT"] = "Ingrese su nombre y correo electrónico en los campos a continuación. Se enviará un mensaje con el código de verificación a esta dirección.";
$MESS["MAIN_MAIL_CONFIRM_CODE_HINT"] = "Se ha enviado un mensaje con el código de verificación a la dirección de correo electrónico especificada. Copie el código y péguelo en el campo a continuación.";
$MESS["MAIN_MAIL_CONFIRM_NAME"] = "Nombre";
$MESS["MAIN_MAIL_CONFIRM_EMAIL"] = "E-mail";
$MESS["MAIN_MAIL_CONFIRM_PUBLIC"] = "Disponible para todos";
$MESS["MAIN_MAIL_CONFIRM_PUBLIC_HINT"] = "Esta dirección estará disponible para todos los empleados que usan CRM";
$MESS["MAIN_MAIL_CONFIRM_CODE_PLACEHOLDER"] = "Ingrese el código de verificación";
$MESS["MAIN_MAIL_CONFIRM_EMPTY_EMAIL"] = "Introducir la dirección de correo electrónico";
$MESS["MAIN_MAIL_CONFIRM_INVALID_EMAIL"] = "Dirección de correo electrónico no válida";
$MESS["MAIN_MAIL_CONFIRM_EMPTY_CODE"] = "Ingrese el código de verificación";
$MESS["MAIN_MAIL_CONFIRM_INVALID_CODE"] = "Código de verificación incorrecto";
$MESS["MAIN_MAIL_CONFIRM_MESSAGE_SUBJECT"] = "Confirmar dirección de e-mail";
?>