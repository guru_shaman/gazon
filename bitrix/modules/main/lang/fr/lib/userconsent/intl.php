<?
$MESS["MAIN_USER_CONSENT_INTL_HINT_FIELD_DEFAULT"] = "Le texte par défaut contiendra :";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_NAME"] = "Nom de l’entreprise";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_NAME_HINT"] = "Exemple : Smith LLC";
$MESS["MAIN_USER_CONSENT_INTL_TYPE_N"] = "Personnalisé";
$MESS["MAIN_USER_CONSENT_INTL_TYPE_S"] = "Standard";
$MESS["MAIN_USER_CONSENT_INTL_NAME"] = "Politique standard de traitement des données personnelles «%language_name%»";
$MESS["MAIN_USER_CONSENT_INTL_LANG_NAME_KZ"] = "Kazakhstan";
?>