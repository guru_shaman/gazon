<?
$MESS["LD_BLOCK_SECTION_OTHER"] = "Інше";
$MESS["LD_BLOCK_SECTION_MENU"] = "Меню";
$MESS["LD_BLOCK_SECTION_FOOTER"] = "Подвал сайту";
$MESS["LD_BLOCK_SECTION_POPULAR"] = "Популярні";
$MESS["LD_BLOCK_SECTION_COVER"] = "Обкладинка";
$MESS["LD_BLOCK_SECTION_ABOUT"] = "Про проект";
$MESS["LD_BLOCK_SECTION_COLUMNS"] = "Колонки";
$MESS["LD_BLOCK_SECTION_TITLE"] = "Заголовок";
$MESS["LD_BLOCK_SECTION_STEPS"] = "Етапи";
$MESS["LD_BLOCK_SECTION_BENEFITS"] = "Переваги";
$MESS["LD_BLOCK_SECTION_GALLERY"] = "Галерея";
$MESS["LD_BLOCK_SECTION_IMAGE"] = "Зображення";
$MESS["LD_BLOCK_SECTION_TARIFFS"] = "Тарифи";
$MESS["LD_BLOCK_SECTION_TEXT"] = "Текстовий блок";
$MESS["LD_BLOCK_SECTION_CONTACTS"] = "Контакти";
$MESS["LD_BLOCK_SECTION_SOCIAL"] = "Соціальні мережі";
$MESS["LD_BLOCK_SECTION_PARTNERS"] = "Партнери";
$MESS["LD_BLOCK_SECTION_FEEDBACK"] = "Відгуки";
$MESS["LD_BLOCK_SECTION_PAGES"] = "Список сторінок";
$MESS["LD_BLOCK_SECTION_TILES"] = "Плитка та посилання";
$MESS["LD_BLOCK_SECTION_PHRASE"] = "Цитата";
$MESS["LD_BLOCK_SECTION_SEPARATOR"] = "Роздільник";
$MESS["LD_BLOCK_SECTION_TEAM"] = "Команда";
$MESS["LD_BLOCK_SECTION_SCHEDULE"] = "Розклад";
$MESS["LD_BLOCK_SECTION_FORMS"] = "CRM-форма";
$MESS["LD_BLOCK_SECTION_LAST"] = "Останні";
$MESS["LD_BLOCK_SECTION_VIDEO"] = "Відео";
?>