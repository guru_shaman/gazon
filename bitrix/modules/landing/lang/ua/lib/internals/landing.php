<?
$MESS["LANDING_TABLE_FIELD_LANDING_CODE"] = "Адреса щодо сайту";
$MESS["LANDING_TABLE_FIELD_LANDING_ACTIVE"] = "Статус сторінки";
$MESS["LANDING_TABLE_FIELD_LANDING_PUBLIC"] = "Публічність";
$MESS["LANDING_TABLE_FIELD_LANDING_TITLE"] = "Назва сторінки";
$MESS["LANDING_TABLE_FIELD_XML_ID"] = "Зовнішній код";
$MESS["LANDING_TABLE_FIELD_DESCRIPTION"] = "Короткий опис";
$MESS["LANDING_TABLE_FIELD_TPL_ID"] = "Шаблон";
$MESS["LANDING_TABLE_FIELD_SITE_ID"] = "Сайт";
$MESS["LANDING_TABLE_FIELD_CREATED_BY_ID"] = "Ідентифікатор створившего користувача";
$MESS["LANDING_TABLE_FIELD_MODIFIED_BY_ID"] = "Ідентифікатор змінившего користувача";
$MESS["LANDING_TABLE_FIELD_DATE_CREATE"] = "Дата створення";
$MESS["LANDING_TABLE_FIELD_DATE_MODIFY"] = "Дата зміни";
$MESS["LANDING_TABLE_ERROR_SLASH_IS_NOT_ALLOWED"] = "Слеш заборонено в адресі лендінгу.";
$MESS["LANDING_TABLE_ERROR_SITE_NOT_FOUND"] = "Сайт не знайдено.";
$MESS["LANDING_TABLE_ERROR_PAGE_LIMIT_REACHED"] = "Досягнуто ліміт сторінок сайтів. Будь ласка, зверніться до техпідтримки.";
$MESS["LANDING_TABLE_ERROR_CANT_BE_EMPTY"] = "Адреса сторінки не може бути порожньою.";
$MESS["LANDING_TABLE_FIELD_DATE_PUBLIC"] = "Дата публікації";
?>