<?
$MESS["LANDING_TABLE_FIELD_TITLE"] = "Назва";
$MESS["LANDING_TABLE_FIELD_ACTIVE"] = "Активність";
$MESS["LANDING_TABLE_FIELD_XML_ID"] = "Зовнішній код";
$MESS["LANDING_TABLE_FIELD_CONTENT"] = "Вміст";
$MESS["LANDING_TABLE_FIELD_AREA_COUNT"] = "Число областей, що включаються";
$MESS["LANDING_TABLE_FIELD_CREATED_BY_ID"] = "Ідентифікатор створившего користувача";
$MESS["LANDING_TABLE_FIELD_MODIFIED_BY_ID"] = "Ідентифікатор змінившего користувача";
$MESS["LANDING_TABLE_FIELD_DATE_CREATE"] = "Дата створення";
$MESS["LANDING_TABLE_FIELD_DATE_MODIFY"] = "Дата зміни";
$MESS["LANDING_TABLE_TPL_HEADER_FOOTER"] = "З шапкою та підвалом";
$MESS["LANDING_TABLE_TPL_SIDEBAR_RIGHT"] = "З сайдбаром праворуч";
?>