<?
$MESS["LANDING_TABLE_FIELD_SITE_CODE"] = "Адреса сайту";
$MESS["LANDING_TABLE_FIELD_SITE_ACTIVE"] = "Статус сайту";
$MESS["LANDING_TABLE_FIELD_SITE_TITLE"] = "Назва сайту";
$MESS["LANDING_TABLE_FIELD_XML_ID"] = "Зовнішній код";
$MESS["LANDING_TABLE_FIELD_DESCRIPTION"] = "Короткий опис";
$MESS["LANDING_TABLE_FIELD_TPL_ID"] = "Шаблон";
$MESS["LANDING_TABLE_FIELD_DOMAIN_ID"] = "Домен";
$MESS["LANDING_TABLE_FIELD_LANDING_ID_INDEX"] = "Головна сторінка";
$MESS["LANDING_TABLE_FIELD_LANDING_ID_404"] = "Сторінка 404";
$MESS["LANDING_TABLE_FIELD_CREATED_BY_ID"] = "Ідентифікатор створившего користувача";
$MESS["LANDING_TABLE_FIELD_MODIFIED_BY_ID"] = "Ідентифікатор змінившего користувача";
$MESS["LANDING_TABLE_FIELD_DATE_CREATE"] = "Дата створення";
$MESS["LANDING_TABLE_FIELD_DATE_MODIFY"] = "Дата зміни";
$MESS["LANDING_TABLE_ERROR_SITE_CODE_IS_NOT_UNIQUE"] = "Адреса сайту не унікальна в рамках домену.";
$MESS["LANDING_TABLE_ERROR_SITE_LIMIT_REACHED"] = "Досягнуто ліміт активних сайтів. Будь ласка, зверніться до техпідтримки.";
$MESS["LANDING_TABLE_ERROR_SITE_IS_NOT_EMPTY"] = "Сайт містить сторінки.";
$MESS["LANDING_TABLE_ERROR_DOMAIN_EXIST"] = "Такий домен вже існує.";
$MESS["LANDING_TABLE_ERROR_DOMAIN_IS_INCORRECT"] = "Ім'я домену некоректно.";
$MESS["LANDING_TABLE_ERROR_CUSTOM_DOMAIN_ISNT_ALLOWED"] = "Власний домен не підтримується на вашому тарифному плані.";
?>