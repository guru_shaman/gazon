<?
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NAME"] = "Cubra con texto e íconos en imágenes de fondo atenuadas";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_CARDS_LANDINGBLOCKNODECARD"] = "Imagen de fondo";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NODES_LANDINGBLOCKNODESUBTITLE"] = "Subtítulo";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NODES_LANDINGBLOCKNODETITLE"] = "Título";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NODES_LANDINGBLOCKNODEICON"] = "Icono";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NODES_LANDINGBLOCKNODEICON_TEXT"] = "Título del icono";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NODES_LANDINGBLOCKNODEBUTTON"] = "Botón";
$MESS["LANDING_BLOCK_46.8.COVER_BGIMG_TITLE_WITH_ICONS_NODES_LANDINGBLOCKNODECARDBGIMG"] = "Imagen de fondo";
?>