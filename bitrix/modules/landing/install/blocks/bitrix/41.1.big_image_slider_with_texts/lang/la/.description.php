<?
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NAME"] = "Gran portada con un deslizador de imagen y texto";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_CARDS_LANDINGBLOCKNODECARD"] = "Block";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARD_ICON"] = "Icono";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARDTITLE"] = "Título";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARDSUBTITLE"] = "Subtítulo";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARDPHOTO"] = "Foto";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARDNAME"] = "Nombre";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARDTEXT"] = "Texto";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODECARDPRICE"] = "Precio";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_NODES_LANDINGBLOCKNODEBGIMG"] = "Imagen de fondo";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_STYLE_LANDINGBLOCKNODECARDTITLE"] = "Título";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_STYLE_LANDINGBLOCKNODECARDSUBTITLE"] = "Subtítulo";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_STYLE_LANDINGBLOCKNODECARDNAME"] = "Nombre";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_STYLE_LANDINGBLOCKNODECARDTEXT"] = "Texto";
$MESS["LANDING_BLOCK_41.BIG_IMAGE_SLIDER_WITH_TEXTS_STYLE_LANDINGBLOCKNODECARDPRICE"] = "Precio";
?>