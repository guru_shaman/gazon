<?
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NAME"] = "Texto con imágenes con viñetas y una imagen a la izquierda";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODE_CARD"] = "Elemento";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODEIMG"] = "Imagen";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODESUBTITLE"] = "Subtítulo";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODETITLE"] = "Título";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODETEXT"] = "Texto";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODECARDICON"] = "Elemento: icono";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODECARDTITLE"] = "Elemento: Subtítulo";
$MESS["LANDING_BLOCK_19.2.FEATURES_WITH_IMG_NODES_LANDINGBLOCKNODECARDTEXT"] = "Elemento: texto";
?>