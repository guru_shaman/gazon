<?
$MESS["LANDING_BLOCK_WEBFORM"] = "Formulaire du CRM";
$MESS["LANDING_BLOCK_WEBFORM_SHOW_HEADER"] = "Titre du formulaire";
$MESS["LANDING_BLOCK_WEBFORM_SHOW_HEADER_Y"] = "Afficher";
$MESS["LANDING_BLOCK_WEBFORM_SHOW_HEADER_N"] = "Masquer";
$MESS["LANDING_BLOCK_WEBFORM_USE_STYLE"] = "Style du format";
$MESS["LANDING_BLOCK_WEBFORM_USE_STYLE_Y"] = "Utiliser le style du conteneur";
$MESS["LANDING_BLOCK_WEBFORM_USE_STYLE_N"] = "Utiliser le style du formulaire GRC";
$MESS["LANDING_BLOCK_WEBFORM_NO_FORM"] = "Aucun formulaire GRC n'est actif";
?>