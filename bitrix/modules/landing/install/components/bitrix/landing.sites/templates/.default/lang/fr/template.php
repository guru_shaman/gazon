<?
$MESS["LANDING_TPL_TITLE"] = "Sites";
$MESS["LANDING_TPL_TITLE_PAGE"] = "Sites";
$MESS["LANDING_TPL_COL_ACTIVE"] = "Actif";
$MESS["LANDING_TPL_COL_ACT_Y"] = "Oui";
$MESS["LANDING_TPL_COL_ACT_N"] = "Non";
$MESS["LANDING_TPL_COL_TITLE"] = "Nom";
$MESS["LANDING_TPL_COL_CREATED"] = "Créé le";
$MESS["LANDING_TPL_COL_MODIFIED"] = "Modifié le";
$MESS["LANDING_TPL_ACTION_EDIT"] = "Configurer le site";
$MESS["LANDING_TPL_ACTION_DELETE"] = "Supprimer le site";
$MESS["LANDING_TPL_ACTION_DELETE_CONFIRM"] = "Voulez-vous vraiment la supprimer ?";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nouveau site";
$MESS["LANDING_TPL_ACTION_ADD_PAGE"] = "Nouveau site";
$MESS["LANDING_TPL_ACTION_ADDPAGE"] = "Créer une page";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Afficher les pages";
$MESS["LANDING_TPL_ACTION_COPYLINK"] = "Copier l'URL";
$MESS["LANDING_TPL_ACTION_COPY"] = "Copier le site";
$MESS["LANDING_TPL_ACTIONS"] = "actions";
?>