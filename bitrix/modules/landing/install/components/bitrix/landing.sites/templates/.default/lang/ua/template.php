<?
$MESS["LANDING_TPL_TITLE"] = "Сайти";
$MESS["LANDING_TPL_COL_ACTIVE"] = "Акт.";
$MESS["LANDING_TPL_COL_ACT_Y"] = "Так";
$MESS["LANDING_TPL_COL_ACT_N"] = "Ні";
$MESS["LANDING_TPL_COL_TITLE"] = "Назва";
$MESS["LANDING_TPL_COL_CREATED"] = "Створено";
$MESS["LANDING_TPL_COL_MODIFIED"] = "Змінено";
$MESS["LANDING_TPL_ACTION_EDIT"] = "Налаштувати сайт";
$MESS["LANDING_TPL_ACTION_DELETE"] = "Видалити сайт";
$MESS["LANDING_TPL_ACTION_DELETE_CONFIRM"] = "Дійсно видалити?";
$MESS["LANDING_TPL_ACTION_ADD"] = "Новий сайт";
$MESS["LANDING_TPL_ACTION_ADDPAGE"] = "Створити сторінку";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Подивитися сторінки";
$MESS["LANDING_TPL_ACTION_COPYLINK"] = "Копіювати посилання";
$MESS["LANDING_TPL_ACTION_COPY"] = "Копіювати сайт";
$MESS["LANDING_TPL_ACTIONS"] = "Дії";
?>