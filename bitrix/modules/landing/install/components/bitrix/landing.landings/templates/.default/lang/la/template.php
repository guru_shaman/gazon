<?
$MESS["LANDING_TPL_TITLE"] = "Sitio web";
$MESS["LANDING_TPL_ACTION_EDIT"] = "Editar ajustes";
$MESS["LANDING_TPL_ACTION_COPY"] = "Copiar página";
$MESS["LANDING_TPL_ACTION_COPY_TITLE"] = "Copiar página al sitio";
$MESS["LANDING_TPL_ACTION_DELETE"] = "Eliminar página";
$MESS["LANDING_TPL_ACTION_DELETE_CONFIRM"] = "¿Quiere eliminar la página?";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nueva pagina";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Editar";
$MESS["LANDING_TPL_ACTION_COPYLINK"] = "Copiar URL";
$MESS["LANDING_TPL_ACTIONS"] = "acciones";
$MESS["LANDING_TPL_PUBLIC"] = "Publicado";
$MESS["LANDING_TPL_UNPUBLIC"] = "No publicado";
?>