<?
$MESS["LANDING_TPL_TITLE"] = "Site";
$MESS["LANDING_TPL_ACTION_EDIT"] = "Editar configurações";
$MESS["LANDING_TPL_ACTION_COPY"] = "Copiar Página";
$MESS["LANDING_TPL_ACTION_COPY_TITLE"] = "Copiar a página para o site";
$MESS["LANDING_TPL_ACTION_DELETE"] = "Excluir Página";
$MESS["LANDING_TPL_ACTION_DELETE_CONFIRM"] = "Você deseja excluir esta página?";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nova Página";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Editar";
$MESS["LANDING_TPL_ACTION_COPYLINK"] = "Copiar URL";
$MESS["LANDING_TPL_ACTIONS"] = "ações";
$MESS["LANDING_TPL_PUBLIC"] = "Publicado";
$MESS["LANDING_TPL_UNPUBLIC"] = "Não publicado";
$MESS["LANDING_TPL_TITLE_PAGE"] = "Site";
?>