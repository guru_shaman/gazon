<?
$MESS["LANDING_TPL_TITLE"] = "Site web";
$MESS["LANDING_TPL_ACTION_EDIT"] = "Ouvrez la fenêtre de réglages";
$MESS["LANDING_TPL_ACTION_COPY"] = "Copier la page";
$MESS["LANDING_TPL_ACTION_COPY_TITLE"] = "Copier la page vers le site";
$MESS["LANDING_TPL_ACTION_DELETE"] = "Supprimer la page";
$MESS["LANDING_TPL_ACTION_DELETE_CONFIRM"] = "Voulez-vous supprimer cette page ?";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nouvelle page";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Éditer";
$MESS["LANDING_TPL_ACTION_COPYLINK"] = "Copier l'URL";
$MESS["LANDING_TPL_ACTIONS"] = "actions";
$MESS["LANDING_TPL_PUBLIC"] = "Publié";
$MESS["LANDING_TPL_UNPUBLIC"] = "Non publié";
$MESS["LANDING_TPL_TITLE_PAGE"] = "Site web";
?>