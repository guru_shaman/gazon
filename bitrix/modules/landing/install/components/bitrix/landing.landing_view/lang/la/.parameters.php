<?
$MESS["LANDING_CMP_PAR_SITE_ID"] = "ID del sitio Web";
$MESS["LANDING_CMP_PAR_LANDING_ID"] = "ID de aterrizaje";
$MESS["LANDING_CMP_PAR_PAGE_URL_URL_SITES"] = "Lista de páginas de sitios";
$MESS["LANDING_CMP_PAR_PAGE_URL_LANDINGS"] = "Lista de páginas de aterrizajes";
$MESS["LANDING_CMP_PAR_PAGE_URL_LANDING_EDIT"] = "Página de edición de aterrizaje.";
$MESS["LANDING_CMP_PAR_PAGE_URL_SITE_EDIT"] = "Enlace a la página del editor del sitio";
?>