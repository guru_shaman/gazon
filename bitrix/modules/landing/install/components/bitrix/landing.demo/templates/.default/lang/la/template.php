<?
$MESS["LANDING_TPL_TITLE"] = "Seleccione una plantilla";
$MESS["LANDING_TPL_ACTION_SELECT"] = "Seleccionar";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nuevo";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Edit";
$MESS["LANDING_TPL_LIMIT_REACHED_TITLE"] = "Límite excedido";
$MESS["LANDING_TPL_SITE_LIMIT_REACHED_TEXT"] = "<p>Su plan actual restringe la cantidad de sitios que puede crear. Tienes que actualizar para crear más sitios.</p><p>Con el plan \"Profesional\", puede crear tantos sitios como quiera.</p>";
$MESS["LANDING_TPL_PAGE_LIMIT_REACHED_TEXT"] = "<p>Su plan actual restringe la cantidad de páginas que puede crear. Tiene que actualizar para crear más páginas.</p><p>Con el plan \"Profesional\", puede crear tantas páginas como quiera.</p>";
?>