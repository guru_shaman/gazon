<?
$MESS["LANDING_TPL_TITLE"] = "Selecione um modelo";
$MESS["LANDING_TPL_ACTION_SELECT"] = "Selecionar";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nova";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Editar";
$MESS["LANDING_TPL_LIMIT_REACHED_TITLE"] = "Limite excedido";
$MESS["LANDING_TPL_SITE_LIMIT_REACHED_TEXT"] = "<p>Seu plano atual restringe o número de sites que você pode criar. Você tem que fazer um upgrade para criar mais sites.</p>
<p>Com o plano \"Profissional\", você pode criar quantos sites quiser.</p>";
$MESS["LANDING_TPL_PAGE_LIMIT_REACHED_TEXT"] = "<p>Seu plano atual restringe o número de páginas que você pode criar. Você tem que fazer um upgrade para criar mais páginas.</p>
<p>Com o plano \"Profissional\", você pode criar quantas páginas quiser.</p>";
?>