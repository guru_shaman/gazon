<?
$MESS["LANDING_TPL_TITLE"] = "Sélectionnez un modèle";
$MESS["LANDING_TPL_ACTION_SELECT"] = "Sélectionner";
$MESS["LANDING_TPL_ACTION_ADD"] = "Nouveau";
$MESS["LANDING_TPL_ACTION_VIEW"] = "Éditer";
$MESS["LANDING_TPL_LIMIT_REACHED_TITLE"] = "Limite dépassée";
$MESS["LANDING_TPL_SITE_LIMIT_REACHED_TEXT"] = "<p>Votre offre actuelle limite le nombre de sites que vous pouvez créer. Vous devez changer d'offre pour créer plus de sites.</p><p>Avec l'offre \"Professional\", vous pouvez autant de sites que vous voulez.</p>";
$MESS["LANDING_TPL_PAGE_LIMIT_REACHED_TEXT"] = "<p>Votre offre actuelle limite le nombre de pages que vous pouvez créer. Vous devez changer d'offre pour créer plus de pages.</p><p>Avec l'offre \"Professional\", vous pouvez autant de pages que vous voulez.</p>";
?>