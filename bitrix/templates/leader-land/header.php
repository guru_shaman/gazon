<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="format-detection" content="telephone=no"/>
        <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon.ico" type="image/x-icon">
        <? $APPLICATION->ShowHead() ?>
        <title><? $APPLICATION->ShowTitle() ?></title>

        <!-- Bootstrap -->
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/bootstrap.css" rel="stylesheet">
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/responsive.css" rel="stylesheet">

        <!-- Links -->
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/camera.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/jquery.fancybox.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/google-map.css">


        <!--JS-->
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-migrate-1.2.1.min.js"></script>




        <!--[if lt IE 9]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
                <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                     alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
            </a>
        </div>
        <script src="<?= SITE_TEMPLATE_PATH ?>js/html5shiv.js"></script>
        <![endif]-->
        <script src='<?= SITE_TEMPLATE_PATH ?>/js/device.min.js'></script>
    </head>
    <body>
        <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
        <div class="page">
            <!--========================================================
                                      HEADER
            =========================================================-->
            <header>  
                <div class="navbar-header">
                    <h1 class="navbar-brand">
                        <a  href="./">Лидер Лэнд
                            <small>Рулонные газоны</small>
                        </a>
                    </h1>
                </div>
                <div id="stuck_container" class="stuck_container">
                    <nav class="navbar navbar-default navbar-static-top ">
                        <div class="container"> 

                            <div class="">  
                                <ul class="navbar-nav sf-menu" data-type="navbar">
                                    <li <? if($APPLICATION->GetCurPage()=='/'):?>class="active"<? endif;?> >
                                        <a href="/">Главная</a>
                                    </li>
                                    <li <? if($APPLICATION->GetCurPage()=='/about/'):?>class="active"<? endif;?>>
                                        <a href="/about">О компании</a>
                                    </li>
                                    <li <? if($APPLICATION->GetCurPage()=='/prices/'):?>class="active"<? endif;?>>
                                        <a href="/prices">Цены и доставка</a>
                                    </li>
                                    <li <? if($APPLICATION->GetCurPage()=='/works/'):?>class="active"<? endif;?>>
                                        <a href="/works">Наши работы</a>
                                    </li>
                                    <li <? if($APPLICATION->GetCurPage()=='/wiki/'):?>class="active"<? endif;?>>
                                        <a href="/wiki">Справочник</a>
                                    </li>
                                    <li <? if($APPLICATION->GetCurPage()=='/contact/'):?>class="active"<? endif;?>>
                                        <a href="/contact">Контакты</a>
                                    </li>
                                </ul>                           
                            </div>

                        </div>
                    </nav>
                </div>  
            </header>
