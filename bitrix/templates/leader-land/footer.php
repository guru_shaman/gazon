<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
IncludeTemplateLangFile(__FILE__);
?> 
<!--========================================================
                            FOOTER
  =========================================================-->
<footer>

    <section>

        <div class="container"> 

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <p class="rights text-uppercase white" style="margin-top: 60px;font-size: 14px;">
                        &#169; <span id="copyright-year"></span><br/>
                        <a href="/" target="_blank">gazon.leader-land.ru</a>
                    </p>   
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">            
                    <address class="text-uppercase">
                        <a class="mail-foot" href="mailto:#">info@leader-land.ru</a><br />
                        <a href="tel:+74953637060">+7 (495) 363 70 60</a><br />
                        <a href="tel:+79647782100">+7 (964) 778 21 00</a><br />
                        <a href="tel:+79031448905">+7 (903) 144 89 05</a><br />
                    </address>
                </div>

                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">



                </div>
            </div>  

        </div> 
    </section>   





</footer>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->         
<script src="<?= SITE_TEMPLATE_PATH ?>/js/bootstrap.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/tm-scripts.js"></script>    
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.fancybox.js"></script>    
<!-- </script> -->
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter47990177 = new Ya.Metrika({id: 47990177, clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true});
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script> 
<noscript>
<div>
    <img src="https://mc.yandex.ru/watch/47990177" style="position:absolute; left:-9999px;" alt="" />
</div>
</noscript> 
<!-- /Yandex.Metrika counter -->
</body>
</html>
