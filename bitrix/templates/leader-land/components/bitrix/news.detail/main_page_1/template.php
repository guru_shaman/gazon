<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container text-center">
    <? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"]): ?>
        <h1 class="white"><?= $arResult["NAME"] ?></h1>
        <br/>
    <? endif; ?>

    <? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
        <p class="ins1 white">
            <? echo $arResult["DETAIL_TEXT"]; ?>
        </p>
    <? endif ?>
    <a href="/about/" class="btn-link">Подробнее</a>
</div>