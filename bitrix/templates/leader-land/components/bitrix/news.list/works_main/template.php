<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<script>
    $(function () {

        $(".img-thumbnail").fancybox();

    });
</script>
<div class="row" style="margin-top: 67px;">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <a href="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" class="img-thumbnail"  data-fancybox-group="1">
                <?
                $arFileTmp = CFile::ResizeImageGet(
                                $arItem["DETAIL_PICTURE"], array("width" => 270, "height" => 180), BX_RESIZE_IMAGE_EXACT
                );
                //print_r($arFileTmp);
                ?>
                <img src="<?= $arFileTmp["src"] ?>" alt="">
                <span class="overlay"></span>
            </a> 
        </div>
    <? endforeach; ?>
</div> 