<?
$MESS["MPL_HAVE_WRITTEN"] = "a écrit:";
$MESS["BPC_MES_DELETE_POST_CONFIRM"] = "tes-vous sûr de vouloir supprimer le commentaire?";
$MESS["B_B_MS_ADD_COMMENT"] = "Ajouter un commentaire";
$MESS["BLOG_C_BUTTON_MORE"] = "Plus";
$MESS["JERROR_NO_MESSAGE"] = "Le texte du commentaire n'a pas été saisi";
$MESS["BLOG_C_REPLY"] = "Répondre";
$MESS["B_B_PC_COM_ERROR"] = "Erreur d'addition du commentaire:";
$MESS["MPL_MES_HREF"] = "Accéder au commentaire";
$MESS["JQOUTE_AUTHOR_WRITES"] = "crit";
$MESS["BPC_MES_SHOW"] = "Afficher";
$MESS["BPC_MES_EDIT"] = "Editer";
$MESS["BLOG_C_HIDE"] = "Cacher les commentaires";
$MESS["BPC_MES_HIDE"] = "Cacher";
$MESS["BLOG_C_VIEW"] = "Consulter les commentaires précédents";
$MESS["B_B_MS_LINK"] = "Lien au commentaire";
$MESS["BPC_MES_DELETE"] = "Supprimer";
$MESS["MPL_SAFE_EDIT"] = "Vous tapez un message. Réinitialiser les modifications?";
$MESS["BLOG_C_VIEW1"] = "Plus de commentaires";
$MESS["BPC_MES_CREATE_TASK"] = "Créer une tâche";
$MESS["MPL_ERROR_OCCURRED"] = "Ceci est une erreur.";
$MESS["MPL_CLOSE"] = "Fermer";
$MESS["MPL_LINK_COPIED"] = "Lien copié";
$MESS["B_B_MS_LINK2"] = "Copier le lien";
?>