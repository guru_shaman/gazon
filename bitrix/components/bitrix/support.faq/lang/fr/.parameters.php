<?
$MESS["RATING_TYPE"] = "Vue des boutons de rating";
$MESS["SHOW_RATING"] = "Activer le classement";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "J'aime (graphique)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "J'aime (textuel)";
$MESS["SUPPORT_FAQ_GROUP_SETTINGS"] = "Paramètres du composant";
$MESS["SUPPORT_RATING_SETTINGS"] = "Réglages des cotes";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "J'aime / Je n'aime pas (graphique)";
$MESS["RATING_TYPE_STANDART_TEXT"] = "J'aime / Je n'aime pas (textuel)";
$MESS["SHOW_RATING_CONFIG"] = "ordinaire";
$MESS["RATING_TYPE_CONFIG"] = "ordinaire";
$MESS["SUPPORT_FAQ_SETTING_EXPAND_LIST"] = "Afficher les sections annexées";
$MESS["SUPPORT_FAQ_SETTING_IBLIST"] = "Blocs d'information";
$MESS["SUPPORT_FAQ_SETTING_SECTIONS_LIST"] = "Liste d'albums";
$MESS["SEF_PAGE_FAQ_DETAIL"] = "Information détaillée";
$MESS["SEF_PAGE_FAQ"] = "Page de la liste commune";
$MESS["SEF_PAGE_FAQ_SECTION"] = "Album";
$MESS["SUPPORT_FAQ_SETTING_IBTYPES"] = "Types des blocs d'information";
$MESS["CP_BSF_CACHE_GROUPS"] = "Prendre en compte les droits d'accès";
$MESS["PATH_TO_USER"] = "Modèle de chemin d'accès  au profil de l'utilisateur";
?>