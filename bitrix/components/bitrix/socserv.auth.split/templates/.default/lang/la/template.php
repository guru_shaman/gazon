<?
$MESS["SS_GET_COMPONENT_INFO"] = "Usted puede vincular su cuenta a:";
$MESS["SS_NAME"] = "Nombre";
$MESS["SS_SOCNET"] = "Redes Sociales";
$MESS["SS_YOUR_ACCOUNTS"] = "Sus cuentas vinculadas:";
$MESS["SS_DELETE"] = "Eliminar";
$MESS["SS_PROFILE_DELETE_CONFIRM"] = "¿Usted está seguro que desea eliminar la cuenta?";
?>