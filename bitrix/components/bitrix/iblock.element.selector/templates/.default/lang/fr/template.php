<?
$MESS["IEST_CURRENT_SELECTED_ITEMS"] = "Sélectionné";
$MESS["IEST_SELECT_ITEMS"] = "Sélectionner";
$MESS["IEST_ERROR_ACCESS_DENIED"] = "Vous ne disposez pas des permissions pour afficher les articles.";
$MESS["IEST_LAST_ELEMENT"] = "Récent";
$MESS["IEST_ELEMENT_SEARCH"] = "Rechercher";
?>