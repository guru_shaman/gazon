<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
?>
<main>   
    <section class="well well8">
        <div class="container">
            <div class="row">
                <h1 style="text-transform: none;">
                    404 - страница не найдена
                </h1>
                <br/>
                <p>
                    Извините, запрашиваемой Вами страницы не существует.
                </p>
                <p>
                    Вы можете перейти на <a href="/" style="color: #78ab06;">главную страницу</a>
                </p>
                <div class="col-md-12 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
    </section>
</main>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>